/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Combobox with autocompletion
 * Date:		12/16/2012
 */

import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * Combobox with autocompletion
 *
 * @author	Kritphong Mongkhonvanit
 * @author	Craig Hammond
 * @version	1.0	12/16/2012
 */
public class AutoComboBox
	extends JComboBox<String>
	implements KeyListener, FocusListener
{
	private static final long serialVersionUID = 1L;

	private String[] items;

	/**
	 * Constructor
	 *
	 * @param	items	Items in the combobox
	 */
	public AutoComboBox(String[] items)
	{
		super(items);

		this.items = items;

		this.setEditable(true);
		this.setMaximumRowCount(5);

		this.getTextField().addKeyListener(this);
		this.getTextField().addFocusListener(this);
	}

	/**
	 * Get a matching candidate from the entries in the combobox
	 */
	private int getMatchCandidate()
	{
		int result = -1;

		for(int i = 0; i < this.items.length && result == -1; i++)
		{
			String listText = this.items[i].toLowerCase();
			String editText = this.getText().toLowerCase();

			// Non case-sensitive match
			if(listText.startsWith(editText) )
			{
				result = i;
			}
		}

		return result;
	}

	/**
	 * Sets the content of the combobox
	 *
	 * @param	items	Items to put in the combobox
	 */
	public void setContent(String[] items)
	{
		this.items = items;

		this.setModel(new JComboBox<String>(this.items).getModel());
	}

	/**
	 * Return the textfield of the combobox
	 */
	private JTextField getTextField()
	{
		return (JTextField)this.getEditor().getEditorComponent();
	}

	/**
	 * Get the text on the text field
	 */
	private String getText()
	{
		return this.getTextField().getText();
	}

	/**
	 * Event handler for keyTyped event
	 *
	 * @param	event	The event object associated with the event
	 */
	public void keyTyped(KeyEvent event)
	{
		String currentText = this.getText();

		this.showPopup();
		this.setSelectedIndex(this.getMatchCandidate());

		this.getTextField().setText(currentText);
	}

	/**
	 * Event handler for keyPressed event
	 *
	 * @param	event	The event object associated with the event
	 */
	public void keyPressed(KeyEvent event)
	{
		// Intentionaly left blank
	}

	/**
	 * Event handler for keyReleased event
	 *
	 * @param	event	The event object associated with the event
	 */
	public void keyReleased(KeyEvent event)
	{
		// Intentionaly left blank
	}

	/**
	 * Event handler for focusGained event
	 *
	 * @param	event	The event object associated with the event
	 */
	public void focusGained(FocusEvent event)
	{
		// Intentionaly left blank
	}

	/**
	 * Event handler for focusLost event
	 *
	 * @param	event	The event object associated with the event
	 */
	public void focusLost(FocusEvent event)
	{
		this.setSelectedIndex(this.getMatchCandidate());
	}
}
