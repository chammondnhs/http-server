/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Contains the implementation of the authenticator system
 * Date:		9/27/2012
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.lang.ClassNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * The tab pane for adding and removing users
 *
 * @author Craig Hammond
 * @author Kritphong Mongkhonvanit
 */
public class UserPane extends GuiPane implements ItemListener, ActionListener
{
	private static final long serialVersionUID = 1L;

	private final String[] choices = {	"Add user",
			  							"Remove user",
		  								"Change Password" };

	private JComboBox<String> comboBox;

	private JTextField usernameTextField;
	private JPasswordField passwordField1;
	private JPasswordField passwordField2;
	private JPasswordField passwordField3;

	private JLabel passwordLabel1;
	private JLabel passwordLabel2;
	private JLabel passwordLabel3;

	private Authenticator authenticator;

	private boolean addUserSelected;

	/**
	 * Constructor
	 */
	public UserPane() throws SQLException, ClassNotFoundException
	{
		GridBagConstraints constraints = new GridBagConstraints();

		this.authenticator		= new Authenticator();
		this.addUserSelected	= true;

		this.comboBox			= new JComboBox<String>(choices);
		this.usernameTextField	= new JTextField(15);
		this.passwordField1		= new JPasswordField(15);
		this.passwordField2		= new JPasswordField(15);
		this.passwordField3		= new JPasswordField(15);
		this.passwordLabel1		= new JLabel("Password:");
		this.passwordLabel2		= new JLabel("Verify Password:");
		this.passwordLabel3		= new JLabel("Password:");

		constraints.fill	= GridBagConstraints.BOTH;
		constraints.anchor	= GridBagConstraints.EAST;
		constraints.weightx	= 0.75;
		constraints.weighty	= 0.5;
		constraints.insets	= new Insets(5, 5, 5, 0);

		this.addToContainer(new JLabel("Action:"), constraints, 0, 0, 1, 1);
		this.addToContainer(new JLabel("Username:"), constraints, 0, 1, 1, 1);
		this.addToContainer(passwordLabel1, constraints, 0, 2, 1, 1);
		this.addToContainer(passwordLabel2, constraints, 0, 3, 1, 1);
		this.addToContainer(passwordLabel3, constraints, 0, 4, 1, 1);

		constraints.weightx = 1.0;
		constraints.weighty = 0.5;
		constraints.insets	= new Insets(5, 0, 5, 5);

		this.comboBox.addItemListener(this);

		this.addToContainer(this.comboBox, constraints, 1, 0, 1, 1);
		this.addToContainer(this.usernameTextField, constraints, 1, 1, 1, 1);
		this.addToContainer(this.passwordField1, constraints, 1, 2, 1, 1);
		this.addToContainer(this.passwordField2, constraints, 1, 3, 1, 1);
		this.addToContainer(this.passwordField3, constraints, 1, 4, 1, 1);

		this.setActionListener(this);

		this.passwordField3.setVisible(false);
		this.passwordLabel3.setVisible(false);
	}

	/**
	 * Event handler for the combobox
	 *
	 * @param	event	The event object associated with the event
	 */
	public void itemStateChanged(ItemEvent event)
	{
		if(this.comboBox.getSelectedIndex() == 0)
		{
			this.passwordLabel1.setText("Password:");
			this.passwordLabel2.setText("Verify Password:");

			this.passwordField2.setVisible(true);
			this.passwordLabel2.setVisible(true);
			this.passwordField3.setVisible(false);
			this.passwordLabel3.setVisible(false);
		}
		else if(this.comboBox.getSelectedIndex() == 1)
		{
			this.passwordLabel1.setText("Password:");

			this.passwordField2.setVisible(false);
			this.passwordLabel2.setVisible(false);
			this.passwordField3.setVisible(false);
			this.passwordLabel3.setVisible(false);
		}
		else if(this.comboBox.getSelectedIndex() == 2)
		{
			this.passwordLabel1.setText("Current Password:");
			this.passwordLabel2.setText("New Password:");
			this.passwordLabel3.setText("Verify New Password:");

			this.passwordField2.setVisible(true);
			this.passwordLabel2.setVisible(true);
			this.passwordField3.setVisible(true);
			this.passwordLabel3.setVisible(true);
		}

		this.validate();

		((JFrame)SwingUtilities.getRoot(this)).pack();

		this.usernameTextField.setText("");
		this.passwordField1.setText("");
		this.passwordField2.setText("");
		this.passwordField3.setText("");
	}

	/**
	 * Handles reset button press
	 */
	private void resetHandler()
	{
		this.setStatus("");
		this.usernameTextField.setText("");
		this.passwordField1.setText("");
		this.passwordField2.setText("");
		this.passwordField3.setText("");
	}

	/**
	 * Handles save button press
	 */
	private void saveHandler()
	throws NoSuchAlgorithmException, SQLException, Exception
	{
		boolean reset		= false;
		String username		= usernameTextField.getText();
		String password1	= new String(passwordField1.getPassword());
		String password2	= new String(passwordField2.getPassword());

		// add user
		if(comboBox.getSelectedIndex() == 0)
		{
			// verify password
			if(password1.equals(password2))
			{
				// Query SQL - Add user
				String passwordHash = Authenticator
										.generateSaltedHash(	username,
																password1 );

				this.authenticator.addUser(username, passwordHash);

				this.setStatus(		username+
									" has been added to database" );
			}
			else
			{
				reset = true;
			}
		}
		// remove user
		else if(comboBox.getSelectedIndex() == 1)
		{
			// verify user is in sql
			if(this.authenticator.verify(username, password1))
			{
				this.authenticator.removeUser(username);

				this.setStatus(	username+
								" has been removed from"+
								" database" );
			}
			else
			{
				this.setStatus(	username+
								" was not found correctly" );

				passwordField1.setText("");
				passwordField2.setText("");
			}
		}
		// change password
		else if(this.comboBox.getSelectedIndex() == 2)
		{
			String password3 = new String(passwordField3.getPassword());

			// verify password
			if(password2.equals(password3))
			{
				// verify they are in sql
				if(this.authenticator.verify(username, password1))
				{
					// Query SQL - Add user
					String passwordHash = Authenticator.generateSaltedHash
															(	username,
																password1 );

					this.authenticator.updatePasswordHash(	username,
															passwordHash );

					this.setStatus(	username+
									" password has been changed" );
				}
				else
				{
					this.setStatus(	username+
									" was not found correctly" );

					this.passwordField1.setText("");
					this.passwordField2.setText("");
					this.passwordField3.setText("");
				}

			}
			else
			{
				reset = true;
			}
		}

		if(reset)
		{
			this.setStatus("Passwords do not match");

			this.passwordField1.setText("");
			this.passwordField2.setText("");
			this.passwordField3.setText("");
		}
	}

	/**
	 * Handle action events
	 *
	 * @param	event	The event object associated with the event
	 */
	public void actionPerformed(ActionEvent event)
	{
		try
		{
			if(event.getSource() == this.getResetButton())
			{
				this.resetHandler();
			}
			else if(event.getSource() == this.getSaveButton())
			{
				this.saveHandler();
			}
			else if(event.getSource() == this.getExitButton())
			{
				System.exit(0);
			}
		}
		catch(Exception exception)
		{
			System.err.println(exception.getMessage());
		}
	}
}
