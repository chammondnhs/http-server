-Setup-
This program requires:
  1. A document root folder
  2. A storage folder; this needs to be under document root
  3. The WebUI files; this needs to be under document root
  4. The SQLite JDBC driver; needs be named sqlite-jdbc.jar and under
     document root
  5. Java must be installed v7 or higher for best results 

The document root and the storage folder can be specified either by editing the
configuration file or through the configuration GUI.

For your convenience, we have provided a zip file with all requirements in
place. If you choose to download the zip, you do not need any other files to
run
the program.

The default database file contains one user with the following login
credentials.

Username: jdoe
Password: pass

You can use this to access the WebUI through the server.

The instructions for starting the server are in the section below.


-Usage-

To run the server:
	No flags
	-s
	--start
	ex: 
		$ java -jar HttpShare.jar --start

To run the configuration Gui:
	-c
	--configure
	ex: 
		$ java -jar HttpShare.jar --configure
	
To add a user from command line:
	-a [user]
	--add-user [user]
	it will then prompt for password
	ex:
		$ java -jar HttpShare.jar -a jdoe
		Enter new password:****
		Retype new password:****
	
To add a user from command line:
	-d username
	--delete-user username
	ex:
		$ java -jar HttpShare.jar -d jdoe
		
For help with the command line:
	-h
	--help
	ex:
		$ java -jar HttpShare.jar -h
		

-Access-
The server will then be accessible through your browser. The default
configuration runs the server on port 8080. This means the server will be
accessible through this URL: http://localhost:8080

-Notes-
1) The server must be running to be able to access it through URL: http://localhost:8080
2) A user must be in a group with access to the file in order to download the file
3) Modifications to group and file settings must be done by command line or the 
	configuration gui. Do not attempt to modify the database.