/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Implementation of the thread used to handle request
 * 				from users
 * Date:		10/8/2012
 */

import java.net.Socket;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Process request received from the client.
 *
 * @author	Kritphong Mongkhonvanit
 * @author	Craig Hammond
 * @version	1.0	10/8/2012
 */
public class ServerThread implements Runnable
{
	private static final String EOL	= "\r\n"; // End-Of-Line character(s) to use

	// Authenticator object for authentication
	private Authenticator authenticator;

	// Directory for storing and retrieving files
	private String filesDirectory	= "files";

	// Enable or disable authentication
	private boolean enableAuth = true;

	private String documentRoot		= ".";		// Path to the document root
	private Socket socket;						// Socket conected to the client

	/**
	 * Constructor
	 *
	 * @param	socket			Socket connected to the client
	 * @param	documentRoot	Path to the document root
	 * @param	filesDirectory	Path to the file storage directory
	 * @param	enable			Enable or disable authentication
	 */
	public ServerThread(	Socket socket,
							String documentRoot,
							String filesDirectory,
							boolean enableAuth )
	throws SQLException, ClassNotFoundException
	{
			Thread thread		= new Thread(this);

			this.socket			= socket;
			this.documentRoot	= documentRoot;
			this.filesDirectory = filesDirectory;
			this.enableAuth		= enableAuth;
			this.authenticator	= new Authenticator();

			thread.start();
	}

	/**
	 * Get MIME type from file extension.
	 *
	 * @param	extension	File extension to get MIME type of
	 */
	private static String getMimeType(String extension)
	{
		// Defaults to application/octet-stream
		String result = "application/octet-stream";

		// Only set proper mimetypes for webpages. The rest will be
		// application/octet-stream so that the browser will show the download
		// dialog.
		if(extension.equals("html") || extension.equals("htm"))
		{
			result = "text/html";
		}
		else if(extension.equals("css"))
		{
			result = "text/css";
		}
		else if(extension.equals("js"))
		{
			result = "application/javascript";
		}

		return result;
	}

	/**
	 * Parse URI. Strips away parameters if there's one.
	 *
	 * @param	URI	The URI to parse
	 */
	private static String parseURI(String URI)
	throws UnsupportedEncodingException
	{
		// Position of the question mark that indicates the beginning of the
		// parameters list of the URI. -1 if there is none.
		int parameterPosition = URI.lastIndexOf("?");

		// Remove parameters
		if(parameterPosition > 0)
		{
			URI = URI.substring(0, URI.lastIndexOf("?"));
		}

		URI = URLDecoder.decode(URI, "UTF-8");

		return URI;
	}

	/**
	 * Get a 'line' from input stream. The 'line' can be any kind of data
	 * terminated with EOL
	 *
	 * @param	input		The stream of data to get a line from
	 */
	private static ArrayList<Byte> getLine(InputStream input)
	throws IOException
	{
		ArrayList<Byte> line	= new ArrayList<Byte>();
		byte current			= 0;
		boolean done			= false;
		boolean newLineCheck	= false;
		int newLineCheckPos		= 1;

		while(!done)
		{
			// the header is all text so it's safe to cast input.read() to char
			current = (byte)input.read();

			if(current == EOL.charAt(0))
			{
				if(EOL.length() == 1)
				{
					done = true;
				}
				else
				{
					newLineCheck = true;
				}
			}
			else if(newLineCheck)
			{
				if(current != EOL.charAt(newLineCheckPos))
				{
					newLineCheck = false;
				}
				else if(newLineCheckPos == EOL.length()-1)
				{
					done = true;
				}
				else
				{
					newLineCheckPos++;
				}
			}

			line.add(current);
		}

		line.subList(line.size()-EOL.length(), line.size()).clear();

		return line;
	}

	/**
	 * Convert array of Objects to byte array
	 *
	 * @param	array	Array of Objects to be converted
	 */
	private static byte[] toByteArray(Object[] array)
	{
		byte[] result = new byte[array.length];

		for(int i = 0; i < result.length; i++)
		{
			result[i] = ((Byte)array[i]).byteValue();
		}

		return result;
	}

	/**
	 * Handles POST requests
	 *
	 * @param	input		The stream of data of the request sans the header
	 * @param	request		The request object
	 */
	private void handlePost(Socket socket, HttpRequest request)
	throws IOException, Exception
	{
		OutputStream socketOut		= socket.getOutputStream();
		InputStream inputStream		= socket.getInputStream();
		BufferedInputStream input	= new BufferedInputStream(inputStream);
		ArrayList<Byte> line		= getLine(input);
		String filename 			= new String();
		FileOutputStream out		= null;
		boolean firstWrite			= true;
		boolean newBoundary			= false;
		boolean endBoundary			= false;

		// loop until the ending boundary is found
		while(!endBoundary)
		{
			String stringLine = new String(toByteArray(line.toArray()));

			// boundary
			if(stringLine.equals("--"+request.getBoundary()))
			{
				newBoundary = true;
			}
			// ending boundary
			else if(stringLine.equals("--"+request.getBoundary()+"--"))
			{
				endBoundary = true;
			}
			else if(newBoundary && stringLine.matches(".*filename=.*"))
			{
				// get filename
				filename = this.filesDirectory+"/";

				filename += stringLine
							.replaceAll(".*filename=\"", "")
							.replaceAll("\".*", "");

				// change filename to the next available filename
				filename = this.nextFilename(filename);

				// add the file to the database
				this.authenticator.addFile(filename.replaceAll("^.*/", ""));

				// initialize output stream
				out = new FileOutputStream(filename);
			}
			else if(newBoundary && stringLine.length() == 0) // end of header
			{
				newBoundary = false;
			}
			else if(!newBoundary) // content
			{
				if(!firstWrite)
				{
					out.write(new String(ServerThread.EOL).getBytes());
				}
				else
				{
					firstWrite = false;
				}

				out.write(toByteArray(line.toArray()));
			}

			if(!endBoundary)
			{
				line = getLine(input);
			}
		}

		out.close();

		// Send response
		byte[] response	= String.format("HTTP/1.1 201 Created%n%n"+
										"<!doctype html>"+
										"<html>"+
										"<head>"+
										"<title>Upload Sucessful</title>"+
										"<meta http-equiv=\"refresh\" "+
										"content=\"0; url=/webui.html\" />"+
										"</head>"+
										"</html>").getBytes();

		socketOut.write(response);

		input.close();
		inputStream.close();
		socketOut.close();
	}

	/**
	 * Finds the next available filename. Appends the revision onto the filename
	 * before extension.
	 *
	 * @param	filename	Filename to be processed
	 */
	private String nextFilename(String filename)
	{
		String result		= filename;
		File testFile		= new File(result);

		while(testFile.exists())
		{
			// Position of the extension
			int extensionPosition	= result.lastIndexOf(".");

			// Position of the opening bracket
			int closingPosition		= result.lastIndexOf(")");

			// Position of the closing bracket
			int openingPosition		= result.lastIndexOf("(");

			// String enclosed by the brackets
			String bracketContent = new String();

			// Found open/close bracket
			if(	openingPosition != -1
				&& closingPosition != -1
				&& extensionPosition != -1
				&& (openingPosition < closingPosition)
				&& (extensionPosition == closingPosition+1) )
			{
				bracketContent = result.substring(	openingPosition+1,
													closingPosition );
			}

			// Previous revisions exist
			if(	bracketContent.length() > 0
				&& bracketContent.matches("^[0-9]*$") )
			{
					int revision = Integer.parseInt(bracketContent)+1;

					result =	result.substring(0, openingPosition+1)
								+revision
								+result.substring(closingPosition);
			}
			// No previous revisions
			else
			{
				// No extension
				if(extensionPosition == -1)
				{
					// Create the first revision
					result += "(1)";
				}
				else
				{
					result =	result.substring(0, extensionPosition)
								+"(1)"
								+result.substring(extensionPosition);
				}
			}

			testFile = new File(result);
		}

		return result;
	}

	/**
	 * Handles GET  requests.
	 *
	 * @param	socket		Socket connected to the client
	 * @param	request		The request object
	 */
	private void handleGet(	Socket socket, HttpRequest request)
	throws IOException, SQLException
	{
		// The full path of the requested file
		String fullPath		=	this.documentRoot+
								ServerThread.parseURI(request.getURI());

		// The file extension of the requested while
		String extension 	= fullPath.substring(fullPath.lastIndexOf(".")+1);

		// File to be served to the client
		File file			= Paths.get(fullPath).toFile();

		// Output stream of the socket
		OutputStream out	= socket.getOutputStream();

		FileInputStream in; // The data stream of the requested file
		int fileSize; 		// The size of the requested file
		int contentLength; 	// The length of the requested content
		int rangeStart;		// The beginning of the requested range
		int rangeEnd;		// The end of the requested range
		byte[] header; 		// The byte array of the response header
		byte[] data; 		// The content of the resquested data
		byte[] response;	// The whole response to be sent to the client

		if(request.getURI().equals("/"))
		{
			response = String.format(	"HTTP/1.1 301 Moved Permanently%n"+
										"Location: /webui.html%n%n" )
										.getBytes();
		}
		else if(file.exists())
		{
			out	= socket.getOutputStream();

			if(file.isFile())
			{
				in			= new FileInputStream(fullPath);
				fileSize	= (int)Files.size(Paths.get(fullPath));

				rangeStart	=	(request.getRangeStart() == -1)?
								0:
								request.getRangeStart();

				rangeEnd	=	(request.getRangeEnd() == -1)?
								fileSize-1:
								request.getRangeEnd();

				contentLength	=	((rangeEnd!=-1)
									?(rangeEnd-rangeStart+1)
									:fileSize-rangeStart);

				if(rangeStart == 0 && rangeEnd == fileSize-1)
				{
					header = String.format(	"HTTP/1.1 200 OK%n"+
											"Content-Type: %s%n%n",
											ServerThread
											.getMimeType(extension) )
											.getBytes();
				}
				else
				{
					header = String.format(	"HTTP/1.1 206 Partial Content%n"+
											"Accept-Ranges: bytes%n"+
											"Content-Length: %d%n"+
											"Content-Range: %d-%d/%d%n"+
											"Content-Type: %s%n%n",
											contentLength,
											rangeStart,
											rangeStart+contentLength-1,
											fileSize,
											ServerThread
											.getMimeType(extension) )
											.getBytes();
				}

				data = new byte[contentLength];

				in.skip(rangeStart);
				in.read(data);

				in.close();
			}
			else // Directory
			{
				String directoryList = new String();

				for(File i : file.listFiles())
				{
					if(i.isFile())
					{
						directoryList += i.getName()+EOL;
					}
				}

				header	= String.format(	"HTTP/1.1 200 OK%n"+
											"Content-Type: text/plain%n%n" )
											.getBytes();

				data	= directoryList.getBytes();
			}

			response = new byte[data.length+header.length];

			System.arraycopy(header, 0, response, 0, header.length);
			System.arraycopy(data, 0, response, header.length, data.length);
		}
		else // Not found
		{
			response = String.format("HTTP/1.1 404 Not Found%n%n").getBytes();
		}

		out.write(response);

		out.close();
	}

	/**
	 * Handles HEAD requests.
	 *
	 * @param	socket		Socket connected to the client
	 * @param	request		The request object
	 */
	private void handleHead(Socket socket, HttpRequest request)
	throws IOException
	{
		// The output stream of the socket
		OutputStream out	= socket.getOutputStream();

		// The full path of the requested file
		String fullPath		=	this.documentRoot+
								ServerThread.parseURI(request.getURI());

		// The extension of the requested file
		String extension 	= fullPath.substring(fullPath.lastIndexOf(".")+1);

		// the response to be sent to the client
		byte[] response		= String.format("HTTP/1.1 200 OK%n"+
											"Accept-Ranges: bytes%n"+
											"Content-Length: %d%n"+
											"Content-Type: %s",
											Files.size(Paths.get(fullPath)),
											ServerThread
											.getMimeType(extension) )
											.getBytes();

		out.write(response);

		out.close();
	}

	/**
	 * Begin the execution of the thread.
	 */
	@Override
	public void run()
	{
		try
		{
			InputStream inputStream = this.socket.getInputStream();

			BufferedInputStream binaryInput	= new BufferedInputStream
													(inputStream);

			HttpRequest request = new HttpRequest(inputStream);

			String filesDirPattern	= "^/"+ this.filesDirectory+"(/.*)?";
			String downloadPattern	= "^/"+ this.filesDirectory+"/.*";
			String filename			= request.getURI().replaceAll("^.*/", "");
			String requestMethod	= request.getMethod();
			String username			= request.getUsername();
			String password			= request.getPassword();

			boolean download = request.getURI().matches(filesDirPattern);

			boolean authenticated =	username != null
									&& this
										.authenticator
										.verify(username, password);

			boolean authorized =	authenticated
									&& this
										.authenticator
										.checkAccessRights(username, filename);

			if(	this.enableAuth 
				&& request.getURI().matches(filesDirPattern) 
				&& !authenticated )
			{
				OutputStream out = this.socket.getOutputStream();

				byte[] response	= String.format ("HTTP/1.1 401 Unauthorized%n"+
												 "WWW-Authenticate: "+
												 "Basic realm=\"login\"%n%n" )
												.getBytes();

				out.write(response);
			}
			else if(	this.enableAuth 
						&& request.getURI().matches(downloadPattern) 
						&& !authorized )
			{
				OutputStream out = this.socket.getOutputStream();

				byte[] response	= String.format("HTTP/1.1 403 Forbidden%n"+
											 	"Content-Type: text/plain%n%n"+
												"You do not have the "+
												"necessary permissions to "+
												"access the requested file." )
												.getBytes();

				out.write(response);
			}
			else if(requestMethod.equals("GET"))
			{
				this.handleGet(this.socket, request);
			}
			else if(requestMethod.equals("POST"))
			{
				this.handlePost(this.socket, request);
			}
			else if(requestMethod.equals("HEAD"))
			{
				this.handleHead(this.socket, request);
			}
			else // malformed/unhandled request
			{
				throw new Exception("Unsupported HTTP request");
			}

			this.socket.close();
			inputStream.close();
			binaryInput.close();
		}
		catch(Exception exception)
		{
			System.err.println(exception.getMessage());
		}
	}
}
