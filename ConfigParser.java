/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Implementation of the server
 * Date:		10/8/2012
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Parses the config file
 *
 * @author	Kritphong Mongkhonvanit
 * @author	Craig Hammond
 * @version	1.0	12/4/2012
 */
public class ConfigParser
{
	// Path to the configuration file
	public static final String CONFIG_PATH = "HttpShare.conf";

	public boolean enableAuth			= true;
	public boolean enableSSL			= true;
	public int port						= 8080;
	public String directoryIndexFile	= "index.html";
	public String documentRoot			= ".";
	public String filesDirectory		= "files";
	public String SSLCertPath			= "cert.p12";

	/**
	 * Parses the value part of a statement in the configuration file. Strips
	 * away quotation marks if there's one.
	 *
	 * @param	configValue	String contaning the statement
	 */
	private String parseConfigValue(String configValue) throws Exception
	{
		String[] tokens	= configValue.split("\"");
		String result	= new String();

		if(tokens.length == 2) // quoted
		{
			result = tokens[1];
		}
		else if(tokens.length == 1) // not quoted
		{
			result = tokens[0];
		}
		else
		{
			throw new Exception("Error parsing config value");
		}

		return result;
	}

	/**
	 * Loads settings from the configuraion file.
	 *
	 * @param	filePath	Path to the configuration file
	 */
	public void loadConfig(String filePath) throws IOException, Exception
	{
		Path configFile			= Paths.get(filePath);
		Charset charset			= Charset.defaultCharset();
		BufferedReader input	= Files.newBufferedReader(configFile, charset);
		String line				= input.readLine();

		while(line != null)
		{
			String[] tokens;

			// Breaks statement into variable part and value part
			tokens = line.split("\\s*=\\s*");

			// Malformed statement
			if(tokens.length != 2)
			{
				throw new Exception("Invalid config statement");
			}

			// Find correspoding variables and set it to the value in the
			// configuration file
			if(tokens[0].equals("DocumentRoot"))
			{
				// Use String.split() to remove quotation marks if there's one
				this.documentRoot = this.parseConfigValue(tokens[1]);
			}
			else if(tokens[0].equals("Port"))
			{
				this.port = Integer.parseInt(this.parseConfigValue(tokens[1]));
			}
			else if(tokens[0].equals("FilesDirectory"))
			{
				this.filesDirectory = this.parseConfigValue(tokens[1]);
			}
			else if(tokens[0].equals("EnableSSL"))
			{
				this.enableSSL = this
									.parseConfigValue(tokens[1])
									.equals("true");
			}
			else if(tokens[0].equals("SSLCertPath"))
			{
				this.SSLCertPath = this.parseConfigValue(tokens[1]);
			}
			else if(tokens[0].equals("EnableAuth"))
			{
				this.enableAuth = this
									.parseConfigValue(tokens[1])
									.equals("true");
			}
			else
			{
				throw new Exception("Unknown config key");
			}

			line = input.readLine();
		}
	}
}
