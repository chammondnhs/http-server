/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	The class conataining the main function
 * Date:		10/8/2012
 */

/**
 * Contains the main function
 *
 * @author	Kritphong Mongkhonvanit
 * @author	Craig Hammond
 * @version	1.0	10/8/2012
 */
public class HttpShare
{
	private static final String START_LONG_FLAG		= "--start";
	private static final String START_SHORT_FLAG	= "-s";

	private static final String CONFIG_LONG_FLAG	= "--configure";
	private static final String CONFIG_SHORT_FLAG	= "-c";

	private static final String ADDUSER_LONG_FLAG	= "--add-user";
	private static final String ADDUSER_SHORT_FLAG	= "-a";

	private static final String DELUSER_LONG_FLAG	= "--delete-user";
	private static final String DELUSER_SHORT_FLAG	= "-d";

	private static final String HELP_LONG_FLAG		= "--help";
	private static final String HELP_SHORT_FLAG		= "-h";

	/**
	 * The main function.
	 *
	 * @param	args	Array of string containing the parameters passed to the
	 *					program
	 */
	public static void main(String[] args)
	{
		try
		{
			if(	args.length == 0 ||
				args[0].equals(START_LONG_FLAG) ||
				args[0].equals(START_SHORT_FLAG) )
			{
				Server server = new Server(); // The server

				server.start();
			}
			else if(args[0].equals(CONFIG_LONG_FLAG) ||
					args[0].equals(CONFIG_SHORT_FLAG) )
			{
				ServerConfig.init();
			}
			else if(args[0].equals(ADDUSER_LONG_FLAG) ||
					args[0].equals(ADDUSER_SHORT_FLAG) )
			{
				Authenticator authenticator	= new Authenticator();

				String username;
				String password;
				String passwordHash;

				username = args[1];

				System.out.print("Enter new password: ");

				password = new String(System.console().readPassword());

				System.out.print("Retype new password: ");

				if(new String(System.console().readPassword()).equals(password))
				{

					passwordHash = Authenticator
											.generateSaltedHash(	username,
																	password );

					authenticator.addUser(username, passwordHash);
				}
				else
				{
					System.out.println("Passwords do not match");
				}
			}
			else if(args[0].equals(DELUSER_LONG_FLAG) ||
					args[0].equals(DELUSER_SHORT_FLAG) )
			{
				Authenticator authenticator	= new Authenticator();

				authenticator.removeUser(args[1]);
			}
			else if(args[0].equals(HELP_LONG_FLAG) ||
					args[0].equals(HELP_SHORT_FLAG) )
			{
				System.out.println(	"HttpShare, a simple HTTP server for file "+
									"sharing.");

				System.out.println(	"\t"+
									CONFIG_SHORT_FLAG+
									", "+
									CONFIG_LONG_FLAG+
									"\t\t\t\tStart the configuration GUI" );

				System.out.println(	"\t"+
									START_SHORT_FLAG+
									", "+
									START_LONG_FLAG+
									"\t\t\t\tStart the server" );

				System.out.println(	"\t"+
									ADDUSER_SHORT_FLAG+
									" [user], "+
									ADDUSER_LONG_FLAG+
									" [user] \t\tAdd a new user" );

				System.out.println(	"\t"+
									DELUSER_SHORT_FLAG+
									" [user], "+
									DELUSER_LONG_FLAG+
									" [user] \tRemove a user" );
			}
			else
			{
				System.out.println(	"Use "+
									HELP_SHORT_FLAG+
									" or "+
									HELP_LONG_FLAG+
									" for help" );
			}
		}
		catch(Exception exception)
		{
			System.err.println(exception.getMessage());
		}
	}
}
