/**
 * Name:		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Implementation of the server
 * Date:		10/8/2012
 */

import java.io.File;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Handles SQL queries
 *
 * @author	Kritphong Mongkhonvanit
 * @author	Craig Hammond
 * @version	1.0	10/24/2012
 */
public class Database
{
	public static final String URI		= "jdbc:sqlite:database.db";
	public static final String DRIVER	= "org.sqlite.JDBC";

	public static final String USER_TABLE			= "users";
	public static final String GROUP_TABLE			= "groups";
	public static final String FILE_TABLE			= "files";
	public static final String USERACCESS_TABLE		= "useraccess";
	public static final String GROUPACCESS_TABLE	= "groupaccess";

	public static boolean initialized = false;

	/**
	 * Constructor
	 */
	public Database() throws SQLException, ClassNotFoundException
	{
		if(!this.initialized)
		{
			Class.forName(DRIVER);

			createDefaultTables();

			this.addUntrackedFiles();
			this.removeOrphanFileEntries();

			this.initialized = true;
		}
	}

	/**
	 * Create the users table
	 */
	private static void createUsersTable()
	throws SQLException, ClassNotFoundException
	{
		Connection connection	= DriverManager.getConnection(URI);
		Statement statement		= connection.createStatement();

		statement.execute(	"CREATE TABLE "+
							USER_TABLE+
							"("+
							"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
							"username VARCHAR,"+
							"password VARCHAR"+
							")" );

		statement.close();
		connection.close();
	}

	/**
	 * Create the groups table
	 */
	private static void createGroupsTable()
	throws SQLException, ClassNotFoundException
	{
		Connection connection	= DriverManager.getConnection(URI);
		Statement statement		= connection.createStatement();

		statement.execute(	"CREATE TABLE "+
							GROUP_TABLE+
							"("+
							"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
							"groupname VARCHAR"+
							")" );

		statement.close();
		connection.close();
	}

	/**
	 * Create the files table
	 */
	private static void createFilesTable()
	throws SQLException, ClassNotFoundException
	{
		Connection connection	= DriverManager.getConnection(URI);
		Statement statement		= connection.createStatement();

		statement.execute(	"CREATE TABLE "+
							FILE_TABLE+
							"("+
							"id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"+
							"filename VARCHAR,"+
							"created DATE"+
							")" );

		statement.close();
		connection.close();
	}

	/**
	 * Creates the user access table
	 */
	private static void createUserAccessTable()
	throws SQLException, ClassNotFoundException
	{
		Connection connection	= DriverManager.getConnection(URI);
		Statement statement = connection.createStatement();

		statement.execute(	"CREATE TABLE "+
							USERACCESS_TABLE+
							"("+
							"user_id INTEGER,"+
							"group_id INTEGER,"+
							"PRIMARY KEY (user_id, group_id),"+
							"FOREIGN KEY (user_id) REFERENCES users(id),"+
							"FOREIGN KEY (group_id) REFERENCES groups(id)"+
							")" );

		statement.close();
		connection.close();
	}

	/**
	 * Create the group access table
	 */
	private static void createGroupAccessTable()
	throws SQLException, ClassNotFoundException
	{
		Connection connection	= DriverManager.getConnection(URI);
		Statement statement		= connection.createStatement();

		statement.execute(	"CREATE TABLE "+
							GROUPACCESS_TABLE+
							"("+
							"group_id INTEGER,"+
							"file_id INTEGER,"+
							"PRIMARY KEY (group_id, file_id),"+
							"FOREIGN KEY (group_id) "+
							"REFERENCES groups(group_id),"+
							"FOREIGN KEY (file_id) "+
							"REFERENCES files(file_id)"+
							")" );

		statement.close();
		connection.close();
	}

	/**
	 * Make sure all required tables exist
	 */
	public static void createDefaultTables()
	throws SQLException, ClassNotFoundException
	{
		Connection connection	= DriverManager.getConnection(URI);

		ResultSet tableList		= connection.getMetaData().getTables(	null,
																		null,
																		"%",
																		null );

		boolean files		= false;
		boolean groups		= false;
		boolean users		= false;
		boolean groupAccess	= false;
		boolean userAccess	= false;

		// Check if table exists
		while(tableList.next())
		{
			String curentTable = tableList.getString(3);

			if(curentTable.equals(USER_TABLE))
			{
				users = true;
			}
			else if(curentTable.equals(GROUP_TABLE))
			{
				groups = true;
			}
			else if(curentTable.equals(FILE_TABLE))
			{
				files = true;
			}
			else if(curentTable.equals(GROUPACCESS_TABLE))
			{
				groupAccess = true;
			}
			else if(curentTable.equals(USERACCESS_TABLE))
			{
				userAccess = true;
			}
		}

		connection.close();
		tableList.close();

		// If table does not exist, create one
		if(!users)
		{
			createUsersTable();
		}

		if(!files)
		{
			createFilesTable();
		}

		if(!groups)
		{
			createGroupsTable();
		}

		if(!groupAccess)
		{
			createGroupAccessTable();
		}

		if(!userAccess)
		{
			createUserAccessTable();
		}
	}

	/**
	 * Finds the user id associated with the owner
	 *
	 * @param	user	The username for which to find ID
	 */
	private static int getUserId(String username) throws SQLException
	{
		Connection connection = DriverManager.getConnection(URI);

		PreparedStatement statement;
		ResultSet resultSet;
		int id;

		statement = connection.prepareStatement(	"SELECT id"+
													" FROM "+
													USER_TABLE+
													" WHERE username = ?" );

		statement.setString(1, username);

		resultSet = statement.executeQuery();

		if(resultSet.next())
		{
			id = resultSet.getInt(1);
		}
		else
		{
			id = -1;
		}

		connection.close();
		resultSet.close();

		return id;
	}

	/**
	 * Finds the group id associated with the groupname
	  *
	 * @param	groupname	The group for which to find ID
	 */
	private static int getGroupId(String groupname) throws SQLException
	{
		Connection connection	= DriverManager.getConnection(URI);

		PreparedStatement statement;
		ResultSet resultSet;
		int id;

		statement = connection.prepareStatement(	"SELECT id"+
													" FROM "+
													GROUP_TABLE+
													" WHERE groupname = ?" );

		statement.setString(1, groupname);

		resultSet = statement.executeQuery();

		if(resultSet.next())
		{
			id = resultSet.getInt(1);
		}
		else
		{
			id = -1;
		}

		statement.close();
		resultSet.close();
		connection.close();

		return id;
	}

	/**
	 * Finds the file id associated with the filename
	 *
	 * @param	filename	The file for which to find ID
	 */
	private static int getFileId(String filename) throws SQLException
	{
		Connection connection = DriverManager.getConnection(URI);

		PreparedStatement statement;
		ResultSet resultSet;
		int id;

		statement = connection.prepareStatement(	"SELECT id"+
													" FROM "+
													FILE_TABLE+
													" WHERE filename = ?" );

		statement.setString(1, filename);

		resultSet = statement.executeQuery();

		if(resultSet.next())
		{
			id = resultSet.getInt(1);
		}
		else
		{
			id = -1;
		}

		connection.close();
		statement.close();
		resultSet.close();

		return id;
	}

	/**
	 * Adds a user into the database
	 *
	 * @param	username	The user to add
	 * @param	password	The hash of the password to add
	 */
	public static void addUser(String username, String password) throws SQLException
	{
		Connection connection	= DriverManager.getConnection(URI);

		PreparedStatement statement;

		statement = connection.prepareStatement(	"INSERT INTO "+
													USER_TABLE+
													"(username,"+
													"password) "+
													"VALUES (?,?)" );

		statement.setString(1, username);
		statement.setString(2, password);

		statement.executeUpdate();

		statement.close();
		connection.close();
	}

	/**
	 * Adds a group into the database
	 *
	 * @param	groupname	The group to add
	 */
	public static void addGroup(String groupname) throws SQLException
	{
		Connection connection	= DriverManager.getConnection(URI);

		PreparedStatement statement;

		statement = connection.prepareStatement(	"INSERT INTO "+
													GROUP_TABLE+
													"(groupname) "+
													"VALUES (?)" );

		statement.setString(1, groupname);

		statement.executeUpdate();

		statement.close();
		connection.close();
	}

	/**
	 * Adds a file into the database
	 *
	 * @param	filename	The file to add
	 */
	public static void addFile(String filename) throws SQLException
	{
		Connection connection	= DriverManager.getConnection(URI);

		PreparedStatement statement;

		statement = connection.prepareStatement(	"INSERT INTO "+
													FILE_TABLE+
													"(filename,"+
													"created) "+
													"values (?,"+
													"current_date)" );

		statement.setString(1, filename);

		statement.executeUpdate();

		statement.close();
		connection.close();
	}

	/**
	 * Adds a group with access to certain files into the database
	 *
	 * @param	groupname	The group to add
	 * @param	filename	The file to associate with the group
	 */
	public static void addGroupAccess(String groupname, String filename)
	throws SQLException
	{
		Connection connection	= DriverManager.getConnection(URI);
		int groupId				= getGroupId(groupname);
		int fileId				= getFileId(filename);

		PreparedStatement statement;

		statement = connection.prepareStatement(	"INSERT INTO "+
													GROUPACCESS_TABLE+
													"(group_id,"+
													"file_id)"+
													"VALUES (?,?)" );

		statement.setInt(1, groupId);
		statement.setInt(2, fileId);

		statement.executeUpdate();

		statement.close();
		connection.close();
	}

	/**
	 * Adds a user with access to groups into the database
	 *
	 * @param	username	The user to add
	 * @param	groupname	The group to associate with the user
	 */
	public static void addUserAccess(String username, String groupname)
	throws SQLException
	{
		Connection connection	= DriverManager.getConnection(URI);
		int groupId				= getGroupId(groupname);
		int userId				= getUserId(username);

		PreparedStatement statement;

		statement = connection.prepareStatement(	"INSERT INTO "+
													USERACCESS_TABLE+
													"(user_id, group_id)"+
													"VALUES (?,?)" );

		statement.setInt(1, userId);
		statement.setInt(2, groupId);

		statement.executeUpdate();

		statement.close();
		connection.close();
	}

	/**
	 * Searches the database in the given table for distinct column names
	 *
	 * @param	table	The table to search within
	 * @param	column	The column to search distinctly
	 */
	private static String[] find(String table, String column) throws SQLException
	{
		Connection connection			= DriverManager.getConnection(URI);
		Statement statement				= connection.createStatement();
		ArrayList<String> resultArray	= new ArrayList<String>();

		ResultSet resultSet;
		String[] result;

		resultSet = statement.executeQuery(	"SELECT DISTINCT "+
											column+
											" FROM "+
											table+
											" ORDER BY "+
											column+
											" ASC" );


		while(resultSet.next())
		{
			resultArray.add(resultSet.getString(column));
		}

		result = resultArray.toArray(new String[resultArray.size()]);

		connection.close();
		resultSet.close();
		statement.close();

		return result;
	}

	/**
	 * Remove a user from database
	 *
	 * @param	username	The username of the user
	 */
	public static void removeUser(String username) throws SQLException
	{
		Connection connection = DriverManager.getConnection(URI);

		PreparedStatement statement;

		statement = connection.prepareStatement(	"DELETE FROM "+
													USERACCESS_TABLE+
													" WHERE user_id = ?" );

		statement.setInt(1, getUserId(username));

		statement.executeUpdate();

		statement = connection.prepareStatement(	"DELETE FROM "+
													USER_TABLE+
													" WHERE username = ?" );

		statement.setString(1, username);

		statement.executeUpdate();

		connection.close();
		statement.close();
	}

	/**
	 * Removes a group from the database
	 *
	 * @param	groupname 	The group to remove
	 */
	public static void removeGroup(String groupname) throws SQLException
	{
		Connection connection = DriverManager.getConnection(URI);

		PreparedStatement statement;

		statement = connection.prepareStatement(	"DELETE FROM "+
													GROUPACCESS_TABLE+
													" WHERE group_id = ?" );

		statement.setInt(1, getGroupId(groupname));

		statement.executeUpdate();

		statement = connection.prepareStatement(	"DELETE FROM "+
													GROUP_TABLE+
													" WHERE groupname = ?" );

		statement.setString(1, groupname);

		statement.executeUpdate();

		connection.close();
		statement.close();
	}

	/**
	 * Removes a file from the database
	 *
	 * @param	filename 	The file to remove
	 */
	public static void removeFile(String filename) throws SQLException
	{
		Connection connection = DriverManager.getConnection(URI);

		PreparedStatement statement;

		statement = connection.prepareStatement(	"DELETE FROM "+
													FILE_TABLE+
													" WHERE filename = ?" );

		statement.setString(1, filename);

		statement.executeUpdate();

		connection.close();
		statement.close();
	}

	/**
	 * Update a user's password hash
	 *
	 * @param	username		The username of the user
	 * @param	passwordHash	The password hash of the user's password
	 */
	public static void updatePasswordHash(String username, String passwordHash)
	throws SQLException
	{
		Connection connection	= DriverManager.getConnection(URI);
		PreparedStatement statement;

		statement = connection.prepareStatement(	"UPDATE "+
													USER_TABLE+
													" SET password = ?"+
													" WHERE username = ?" );

		statement.setString(1, passwordHash);
		statement.setString(2, username);

		statement.executeUpdate();

		connection.close();
		statement.close();
	}

	/**
	 * Get a user's password hash
	 *
	 * @param	username	The username of the user
	 */
	public static String getPasswordHash(String username) throws SQLException
	{
		Connection connection = DriverManager.getConnection(URI);

		PreparedStatement statement;
		ResultSet resultSet;
		String result;

		statement = connection.prepareStatement(	"SELECT password"+
													" FROM "+
													USER_TABLE+
													" WHERE username = ?" );

		statement.setString(1, username);

		resultSet	= statement.executeQuery();
		result		= (resultSet.isBeforeFirst())?resultSet.getString(1):"";

		resultSet.close();
		statement.close();
		connection.close();

		return result;
	}

	/**
	 * Add files not in database to the database
	 */
	public static void addUntrackedFiles() throws SQLException
	{
		String filesDirectory		= new ConfigParser().filesDirectory;
		String[] databaseFileList	= getFileList();

		File[] directoryFileList = Paths
										.get(filesDirectory)
										.toFile()
										.listFiles();

		for(File i : directoryFileList)
		{
			boolean found = false;

			for(String j : databaseFileList)
			{
				if(i.getName().equals(j))
				{
					found = true;
				}
			}

			if(!found)
			{
				addFile(i.getName());
			}
		}
	}

	/**
	 * Remove files that exist in database but not in the actual directory
	 */
	public static void removeOrphanFileEntries() throws SQLException
	{
		String filesDirectory		= new ConfigParser().filesDirectory;
		String[] databaseFileList	= getFileList();

		File[] directoryFileList = Paths
										.get(filesDirectory)
										.toFile()
										.listFiles();

		for(String i : databaseFileList)
		{
			boolean found = false;

			for(File j : directoryFileList)
			{
				if(i.equals(j.getName()))
				{
					found = true;
				}
			}

			if(!found)
			{
				removeFile(i);
			}
		}
	}

	/**
	 * Checks whether the given user has access to the given file
	 *
	 * @param	username	The user
	 * @param	filename	The file
	 */
	public static boolean checkAccessRights(String username, String filename)
	throws SQLException
	{
		ArrayList<Integer> userGroupIdList = getUserGroupIds(username);
		ArrayList<Integer> fileGroupIdList = getFileGroupIds(filename);

		boolean result = false;

		for(int i = 0; i < userGroupIdList.size() && !result; i++)
		{
			for(int j = 0; j < fileGroupIdList.size() && !result; j++)
			{
				if(userGroupIdList.get(i) == fileGroupIdList.get(j))
				{
					result = true;
				}
			}
		}

		return result;
	}

	/**
	 * Get all Ids of groups associated with the given user
	 *
	 * @param	username	The user
	 */
	public static ArrayList<Integer> getUserGroupIds(String username)
	throws SQLException
	{
		Connection connection		= DriverManager.getConnection(URI);
		ArrayList<Integer> result	= new ArrayList<Integer>();

		PreparedStatement statement;
		ResultSet resultSet;

		statement = connection.prepareStatement(	"SELECT group_id"+
													" FROM "+
													USERACCESS_TABLE+
													" WHERE user_id = ?" );

		statement.setInt(1, getUserId(username));

		resultSet = statement.executeQuery();

		while(resultSet.next())
		{
			result.add(resultSet.getInt(1));
		}

		resultSet.close();
		statement.close();
		connection.close();

		return result;
	}

	/**
	 * Get all group IDs associated with the given file
	 *
	 * @param	filename	The file
	 */
	public static ArrayList<Integer> getFileGroupIds(String filename)
	throws SQLException
	{
		Connection connection		= DriverManager.getConnection(URI);
		ArrayList<Integer> result	= new ArrayList<Integer>();

		PreparedStatement statement;
		ResultSet resultSet;

		statement = connection.prepareStatement(	"SELECT group_id"+
													" FROM "+
													GROUPACCESS_TABLE+
													" WHERE file_id = ?" );

		statement.setInt(1, getFileId(filename));

		resultSet = statement.executeQuery();

		while(resultSet.next())
		{
			result.add(resultSet.getInt(1));
		}

		resultSet.close();
		statement.close();
		connection.close();

		return result;
	}

	/**
	 * Returns an array of all users
	 */
	public static String[] getUserList() throws SQLException
	{
		return find(USER_TABLE, "username");
	}

	/**
	 * Returns an array of all groups
	 */
	public static String[] getGroupList() throws SQLException
	{
		return find(GROUP_TABLE, "groupname");
	}

	/**
	 * Returns an array of all files
	 */
	public static String[] getFileList() throws SQLException
	{
		return find(FILE_TABLE, "filename");
	}
}
