/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Contains the implementation of the authenticator system
 * Date:		12/27/2012
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.lang.ClassNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JButton;

/**
 * The tab pane for modifying groups
 *
 * @author Craig Hammond
 * @author Kritphong Mongkhonvanit
 */
public class GroupPane extends GuiPane implements ActionListener
{
	private static final long serialVersionUID = 1L;

	private AutoComboBox deleteGroupBox;

	private JButton deleteGroupButton;
	private JButton newGroupButton;

	private JTextField newGroupField;

	private Authenticator authenticator;

	/**
	 * Constructor
	 */
	public GroupPane() throws SQLException, ClassNotFoundException
	{
		GridBagConstraints constraints = new GridBagConstraints();

		this.authenticator		= new Authenticator();
		this.newGroupButton		= new JButton("Create New Group");
		this.deleteGroupButton	= new JButton("Delete Group");
		this.newGroupField		= new JTextField(15);

		this.deleteGroupBox = new AutoComboBox
									(this.authenticator.getGroupList());

		this.getSaveButton().setVisible(false);
		this.getResetButton().setVisible(false);
		this.getExitButton().setVisible(false);

		constraints.fill	= GridBagConstraints.BOTH;
		constraints.anchor	= GridBagConstraints.EAST;
		constraints.weightx	= 0.25;
		constraints.weighty	= 0.5;
		constraints.insets	= new Insets(5, 5, 5, 0);

		this.newGroupButton.addActionListener(this);
		this.deleteGroupButton.addActionListener(this);

		this.addToContainer(this.newGroupButton, constraints, 0, 0, 1, 1);
		this.addToContainer(this.newGroupField, constraints, 1, 0, 1, 1);

		constraints.weightx	= 0.75;
		constraints.weighty	= 0.5;
		constraints.insets	= new Insets(15, 5, 15, 0);

		this.addToContainer(this.deleteGroupButton, constraints, 0, 1, 1, 1);
		this.addToContainer(this.deleteGroupBox, constraints, 1, 1, 1, 1);

		this.setActionListener(this);
	}

	/**
	 * Handles reset button press
	 */
	private void resetHandler() throws SQLException
	{
		newGroupField.setText("");
		update();
	}

	/**
	 * Handles new groups
	 */
	public void newGroupHandler() throws SQLException
	{
		if ( newGroupField.getText().compareTo("") != 0 )
		{
			this.authenticator.addGroup(newGroupField.getText());

			this.setStatus(newGroupField.getText()+" has been created");
		}

		this.resetHandler();
	}

	/**
	 * Handles removing groups
	 */
	public void deleteGroupHandler() throws SQLException
	{
		if(deleteGroupBox.getSelectedIndex() != -1)
		{
			this.setStatus(	this.deleteGroupBox.getSelectedItem().toString()+
							" has been deleted" );

			this.authenticator.removeGroup
							(deleteGroupBox.getSelectedItem().toString());
		}

		this.update();
	}

	/**
	 * Updates the combo boxes
	 */
	public void update() throws SQLException
	{
		this.deleteGroupBox.setContent(this.authenticator.getGroupList());
	}

	/**
	 * Handles actionlistener
	 */
	public void actionPerformed(ActionEvent event)
	{
		try
		{
			if(event.getSource() == this.getResetButton())
			{
				this.resetHandler();
			}
			else if (event.getSource() == this.newGroupButton)
			{
				this.newGroupHandler();
			}
			else if (event.getSource() == this.deleteGroupButton)
			{
				this.deleteGroupHandler();
			}
			else if(event.getSource() == this.getExitButton())
			{
				System.exit(0);
			}
		}
		catch(Exception exception)
		{
			System.err.println(exception.getMessage());
		}
	}
}
