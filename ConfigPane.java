/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Contains the implementation of the authentication system
 * Date:		9/27/2012
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * The tab pane for editing server configurations
 *
 * @author Craig Hammond
 * @author Kritphong Mongkhonvanit
 */
public class ConfigPane extends GuiPane implements ActionListener, FocusListener
{
	private static final long serialVersionUID = 1L;

	private static final int SPACING = 3;

	private ConfigParser config;

	private JTextField portTextField;

	private JFileChooser docRootChooser;
	private JButton docRootButton;
	private JTextField docRootTextField;

	private JFileChooser filesDirChooser;
	private JButton filesDirButton;
	private JTextField filesDirTextField;

	private JFileChooser certPathChooser;
	private JButton certPathButton;
	private JTextField certPathTextField;

	private JCheckBox sslCheckBox;
	private JCheckBox authCheckBox;

	/**
	 * Constructor
	 */
	public ConfigPane() throws IOException, FileNotFoundException, Exception
	{
		super();

		this.config = new ConfigParser();

		File currentConfig				= new File("HttpShare.conf");
		GridBagConstraints constraints	= new GridBagConstraints();

		constraints.weightx = 0.5;
		constraints.weighty = 0.5;
		constraints.anchor	= GridBagConstraints.EAST;
		constraints.fill	= GridBagConstraints.BOTH;
		constraints.insets	= new Insets(5, 5, 5, 0);

		// Add labels
		this.addToContainer
				(new JLabel("Document Root: "), constraints, 0, 2, 1, 1);

		this.addToContainer
				(new JLabel("Files Directory: "), constraints, 0, 1, 1, 1);

		this.addToContainer
				(new JLabel("Port: "), constraints, 0, 0, 1, 1);

		this.addToContainer
				(new JLabel("SSL Certificate: "), constraints, 0, 3, 1, 1);

		this.addToContainer
				(new JLabel("Enable SSL"), constraints, 1, 4, 2, 1);

		this.addToContainer
				(new JLabel("Enable authentication"), constraints, 1, 5, 2, 1);

		constraints.gridwidth	= 1;
		constraints.insets		= new Insets(5, 0, 5, 5);

		// Document Root Button
		this.docRootButton = new JButton("Open");
		this.docRootButton.addActionListener(this);
		this.addToContainer(this.docRootButton, constraints, 1, 2, 1, 1);

		// Files Directory Button
		this.filesDirButton = new JButton("Open");
		this.filesDirButton.addActionListener(this);
		this.addToContainer(filesDirButton, constraints, 1, 1, 1, 1);

		// SSL Certificate Button
		this.certPathButton = new JButton("Open");
		this.certPathButton.addActionListener(this);
		this.addToContainer(certPathButton, constraints, 1, 3, 1, 1);

		// Spacing
		this.addToContainer(new JPanel(), constraints, 1, 2, 1, 1);

		// Set up the right hand side
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.insets	= new Insets(8, 0, 8, 5);

		// DocumentRoot text field
		this.docRootTextField = new JTextField(35);
		this.addToContainer(this.docRootTextField, constraints, 2, 2, 1, 1);

		// Files Directory text Field
		this.filesDirTextField = new JTextField(35);
		this.addToContainer(this.filesDirTextField, constraints, 2, 1, 1, 1);

		// Port Text Field
		this.portTextField = new JTextField(35);
		this.portTextField.addFocusListener(this);
		this.addToContainer(this.portTextField, constraints, 2, 0, 1, 1);

		// SSL certificate path
		this.certPathTextField = new JTextField(35);
		this.addToContainer(this.certPathTextField, constraints, 2, 3, 1, 1);

		constraints.fill	= GridBagConstraints.NONE;
		constraints.anchor	= GridBagConstraints.EAST;

		// SSL check box
		this.sslCheckBox = new JCheckBox();
		this.addToContainer(this.sslCheckBox, constraints, 0, 4, 1, 1);

		// Authentication check box
		this.authCheckBox = new JCheckBox();
		this.addToContainer(this.authCheckBox, constraints, 0, 5, 1, 1);

		// Set up the File Choosers

		// Document Root File Chooser
		this.docRootChooser = new JFileChooser();

		this.docRootChooser.setFileSelectionMode
								(JFileChooser.DIRECTORIES_ONLY);

		this.docRootChooser.setAcceptAllFileFilterUsed(false);

		// Files Directory File Chooser
		this.filesDirChooser = new JFileChooser();

		this.filesDirChooser.setFileSelectionMode
								(JFileChooser.DIRECTORIES_ONLY);

		this.filesDirChooser.setAcceptAllFileFilterUsed(false);

		// SSL Certificate File Chooser
		this.certPathChooser = new JFileChooser();

		this.certPathChooser.setFileSelectionMode
								(JFileChooser.FILES_ONLY);

		this.certPathChooser.setAcceptAllFileFilterUsed(true);

		if(currentConfig.exists())
		{
			this.config.loadConfig("HttpShare.conf");
		}
		else
		{
			this.config.documentRoot	= ".";
			this.config.filesDirectory	= "files";
			this.config.port			= 8080;

			this.saveHandler();
		}

		this.docRootChooser.setCurrentDirectory
								(new File(this.config.documentRoot));

		this.filesDirChooser.setCurrentDirectory
								(new File(this.config.filesDirectory));

		this.certPathChooser.setCurrentDirectory
								(new File(this.config.SSLCertPath));

		this.portTextField.setText(Integer.toString(this.config.port));
		this.docRootTextField.setText(this.config.documentRoot);
		this.filesDirTextField.setText(this.config.filesDirectory);
		this.certPathTextField.setText(this.config.SSLCertPath);

		this.sslCheckBox.setSelected(this.config.enableSSL);
		this.authCheckBox.setSelected(this.config.enableAuth);

		this.setActionListener(this);
	}

	/**
	 * Handles reset button press
	 */
	private void resetHander() throws IOException, Exception
	{
		this.config.loadConfig("HttpShare.conf");

		this.portTextField.setText(Integer.toString(this.config.port));
		this.docRootTextField.setText(this.config.documentRoot);
		this.filesDirTextField.setText(this.config.filesDirectory);

		this.setStatus("Reset");
	}

	/**
	 * Handles save button press
	 */
	private void saveHandler() throws IOException
	{
		File newFile	= new File("HttpShare.conf");
		FileWriter out 	= new FileWriter(newFile);

		if(this.authCheckBox.isSelected() && !this.sslCheckBox.isSelected())
		{
			JOptionPane.showMessageDialog(	this,
											"You're enabling authentication "+
											"without SSL. This means "+
											System.lineSeparator()+
											"authentication will be done in "+
											"the clear. This is usually a bad "+
											System.lineSeparator()+
											"idea unless you only intend to "+
											"authenticate over trusted "+
											System.lineSeparator()+
											"network.",
											"Warning",
											JOptionPane.WARNING_MESSAGE );
		}

		out.write(	"DocumentRoot = \""+
					this.config.documentRoot+
					"\""+
					System.lineSeparator()+
					"FilesDirectory = \""+
					this.config.filesDirectory+
					"\""+
					System.lineSeparator()+
					"Port = \""+
					Integer.toString(this.config.port)+
					"\""+
					System.lineSeparator()+
					"SSLCertPath = \""+
					this.config.SSLCertPath+
					"\""+
					System.lineSeparator()+
					"EnableSSL = \""+
					Boolean.toString(this.sslCheckBox.isSelected())+
					"\""+
					System.lineSeparator()+
					"EnableAuth = \""+
					Boolean.toString(this.authCheckBox.isSelected())+
					"\"" );

		out.close();

		this.setStatus("Save Successful");
	}

	/**
	 * Handles file selection buttons
	 *
	 * @param	fileChooser	The file chooser associated with the button
	 * @param	textField	The text field associated with the button
	 */
	private void chooserButtonHandler(	JFileChooser fileChooser,
										JTextField textField )
	{
		int option = fileChooser.showOpenDialog(this);

		String directory;

		if(option == JFileChooser.APPROVE_OPTION)
		{
			directory = fileChooser
							.getSelectedFile()
							.toString();

			textField.setText(directory);
		}
	}

	/**
	 * Handles focus lost event of the text field for port
	 */
	private void portFocusLostHandler()
	{
		String portString	= this.portTextField.getText();
		int port			= -1;

		if(portString.matches("^[0-9]{1,5}$"))
		{
			port = Integer.parseInt(portString);
		}

		if(port >= 0 && port <= 65535)
		{
			this.config.port = Integer.parseInt(portTextField.getText());
		}
		else
		{
			JOptionPane.showMessageDialog(	this,
											"Invalid port",
											"Error",
											JOptionPane.ERROR_MESSAGE );

			this.portTextField.setText(Integer.toString(this.config.port));
		}
	}

	/**
	 * Event handler for focus gains
	 *
	 * @param	event	The event object associated with the event
	 */
	 public void focusGained(FocusEvent event)
	 {
	 	// intentionally left blank
	 }

	/**
	 * Event handler for focus losses
	 *
	 * @param	event	The event object associated with the event
	 */
	 public void focusLost(FocusEvent event)
	 {
	 	if(event.getSource() == this.portTextField)
		{
			this.portFocusLostHandler();
		}
	 }

	/**
	 * Event handler for button presses
	 *
	 * @param	event	The event object associated with the event
	 */
	public void actionPerformed(ActionEvent event)
	{
		try
		{
			if(event.getSource() == this.getResetButton())
			{
				this.resetHander();
			}
			else if(event.getSource() == this.getSaveButton())
			{
				this.saveHandler();
			}
			else if(event.getSource() == this.getExitButton())
			{
				System.exit(0);
			}
			else if(event.getSource() == this.docRootButton)
			{
				this.chooserButtonHandler(	this.docRootChooser,
											this.docRootTextField );

				this.config.documentRoot = this.docRootTextField.getText();
			}
			else if(event.getSource() == this.filesDirButton)
			{
				this.chooserButtonHandler(	this.filesDirChooser,
											this.filesDirTextField );

				this.config.filesDirectory = this.filesDirTextField.getText();
			}
			else if(event.getSource() == this.certPathButton)
			{
				this.chooserButtonHandler(	this.certPathChooser,
											this.certPathTextField );

				this.config.SSLCertPath = this.certPathTextField.getText();
			}
		}
		catch(Exception exception)
		{
			System.err.println(exception.getMessage());
		}
	}
}
