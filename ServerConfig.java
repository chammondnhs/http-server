/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Contains the implementation of the authentication system
 * Date:		9/27/2012
 */

import java.io.IOException;
import java.lang.ClassNotFoundException;
import java.sql.SQLException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 * The GUI for configuring the server
 *
 * @author Craig Hammond
 * @author Kritphong Mongkhonvanit
 */
class ServerConfig extends JPanel implements ChangeListener
{
	private static final long serialVersionUID = 1L;

	private ConfigPane 		configPane;
	private UserPane 		userPane;
	private GroupPane		groupPane;
	private AccessPane		accessPane;
	private JTabbedPane 	tabbedPane;

	/**
	 * Constructor
	 */
	public ServerConfig()
	throws IOException, SQLException, ClassNotFoundException, Exception
	{
		super();

		this.tabbedPane	= new JTabbedPane(JTabbedPane.TOP);
		this.configPane	= new ConfigPane();
		this.userPane	= new UserPane();
		this.groupPane 	= new GroupPane();
		this.accessPane = new AccessPane();

		this.tabbedPane.addChangeListener(this);

		this.tabbedPane.add("Configuration", configPane);
		this.tabbedPane.add("Manage Users", userPane);
		this.tabbedPane.add("Manage Groups", groupPane);
		this.tabbedPane.add("Manage Access", accessPane);

		this.add(tabbedPane);
	}

	/**
	 * Event handler for state changes
	 *
	 * @param	 event	The event object associated with the event
	 */
	public void stateChanged(ChangeEvent event)
	{
		try
		{
			int selectedPane = this.tabbedPane.getSelectedIndex();

			if(selectedPane == 2 || selectedPane == 3)
			{
				this.accessPane.update();
				this.groupPane.update();
			}
		}
		catch(Exception exception)
		{
			System.out.println(exception.getMessage());
		}
	}

	/*
	 * Initialize the GUI
	 */
	public static void init()
	throws ClassNotFoundException, IOException, SQLException, Exception
	{
		ServerConfig serverConfig	= new ServerConfig();
		JFrame frame				= new JFrame();

		frame.add(serverConfig);

		frame.pack();

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Server Management");
		frame.setResizable(false);
		frame.setVisible(true);
	}
}
