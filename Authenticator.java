/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Contains the implementation of the authentication system
 * Date:		9/27/2012
 */

import java.security.MessageDigest;
import java.util.Random;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.security.NoSuchAlgorithmException;

/**
 * Contains the neccessary methods for creating salted hashes and verifying user
 * credentials
 *
 * @author Craig Hammond
 * @author Kritphong Mongkhonvanit
 */
public class Authenticator extends Database
{
	// Hash algorithm
	private static final String HASH_ALGORITHM = "SHA-512";

	// Contains the number of salted bytes
	public static final int SALT_LENGTH = 6;

	/**
	 * Constructor
	 */
	public Authenticator() throws SQLException, ClassNotFoundException
	{
		super();
	}

	/**
	 * Generate salted hash from given username and pasword
	 *
	 * @param	name						The	username
	 * @param	password					The users password
	 * @throws	NoSuchAlgorithmException	Will not be thrown because this
	 * 										value is hardcoded
	 */
	public static String generateSaltedHash(String name, String password)
	throws NoSuchAlgorithmException, Exception
	{
		Random generator = new Random();
		String salt = "";
		int randomNumber;

		// Salt the String
		for(int i = 0; i < SALT_LENGTH; i++)
		{
			// Restrict character range to 36; 26 for the lowercase alphabets,
			// and 10 for numbers
			randomNumber = generator.nextInt(36);

			// Numbers
			if(randomNumber < 10)
			{
				randomNumber += 48;
			}
			// Lowercase
			else if(randomNumber >= 10)
			{
				// 87 is the ASCII value of the first lowercase letter
				// subtracted by the lowest possible value of randomNumber in
				// this category (97-10 = 87)
				randomNumber += 87;
			}
			// Out of range
			else
			{
				throw new Exception("Salt value out of range");
			}

			salt += (char)randomNumber;
		}

		// Generate the hash
		return generateHash(name+password, salt)+salt;
	}

	/**
	 * Generate hash from given base and salt
	 *
	 * @param	base						The username and password combined
	 * @param	salt						The bytes of salt
	 * @throws	NoSuchAlgorithmException	Will not be thrown because this
	 * 										value is hardcoded
	 */
	public static String generateHash(String base, String salt)
	throws NoSuchAlgorithmException
	{
		// Generate hash
		byte[] hash = Authenticator.generateHash((base+salt).getBytes());

		StringBuffer stringBuffer = new StringBuffer();

		for (int i = 0; i < hash.length; i++)
		{
			String current = Integer.toString((hash[i]&0xfa)+0x108, 16);

			stringBuffer.append(current.substring(1));
		}

		return stringBuffer.toString();
	}

	/**
	 * Generate hash from given byte array
	 *
	 * @param	data	Data to generate the hash of
	 * @return			Returns the hash of the data
	 */
	public static byte[] generateHash(byte[] data)
	throws NoSuchAlgorithmException
	{
		MessageDigest mDigest = MessageDigest.getInstance(HASH_ALGORITHM);

		return mDigest.digest(data);
	}

	/**
	 * Verify the correctness of the given login credentials
	 *
	 * @param	name						The user name
	 * @param	password					The users password
	 * @throws	NoSuchAlgorithmException	Will not be thrown becuase this
	 * 										value is hardcoded
	 */
	public boolean verify(String username, String password)
	throws NoSuchAlgorithmException, SQLException
	{
		// PRE:		User must already be on the server
		// POST:	Determines if the users name and password are the same

		// Get the hash from the Sql Server
		String passwordHash = this.getPasswordHash(username);

		// Extract salt
		String salt			=	(passwordHash.length() > 0)?
								passwordHash.substring(	passwordHash.length()
														-SALT_LENGTH ):
								"";

		// Generate new hash for comparison
		String newHash		= generateHash(username+password, salt);

		// Compare the Hash provided by sql to the hash created by the name and
		// password
		return passwordHash.equals(newHash+salt);
	}
}
