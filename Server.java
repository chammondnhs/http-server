/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Implementation of the server
 * Date:		10/8/2012
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStore;
import java.sql.SQLException;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

/**
 * Accepts connections and pass them to ServerThread instances, which will be
 * created dynamically.
 *
 * @author	Kritphong Mongkhonvanit
 * @author	Craig Hammond
 * @version	1.0	10/8/2012
 */
public class Server
{
	private static final String SSL_ALGO		= "TLSv1.1";
	private static final String KEYMANAGER_ALGO	= "SunX509";
	private static final String KEYSTORE_TYPE	= "pkcs12";

	// Configuration parser
	private ConfigParser config;

	// Server socket used to listen to connections
	private ServerSocket serverSocket;

	/**
	 * Constructor - Load settings from the configuration file.
	 */
	public Server() throws IOException, Exception
	{
		File filesDirectory;

		this.config = new ConfigParser();

		this.config.loadConfig(ConfigParser.CONFIG_PATH);

		filesDirectory = new File(this.config.filesDirectory);

		if(!filesDirectory.exists() && !filesDirectory.mkdirs())
		{
			throw new Exception("Unable to create storage directory");
		}

		if(this.config.enableSSL)
		{
			this.serverSocket = this.createSSLServerSocket();
		}
		else
		{
			this.serverSocket = new ServerSocket(this.config.port);
		}
	}

	/**
	 * Create and return an SSLServerSocket
	 */
	private ServerSocket createSSLServerSocket() throws	Exception
	{
		char[] password;

		SSLContext context	= SSLContext.getInstance(SSL_ALGO);
		KeyStore keyStore	= KeyStore.getInstance(KEYSTORE_TYPE);

		KeyManagerFactory keyFactory = KeyManagerFactory
											.getInstance(KEYMANAGER_ALGO);

		System.out.print("Enter certificate password: ");

		password = System.console().readPassword();

		keyStore.load(	new FileInputStream(this.config.SSLCertPath),
						password );

		keyFactory.init(keyStore, password);

		context.init(keyFactory.getKeyManagers(), null, null);

		return context
				.getServerSocketFactory()
				.createServerSocket(this.config.port);
	}

	/**
	 * Starts the server.
	 */
	public void start() throws IOException, SQLException, ClassNotFoundException
	{
		while(true)
		{
			Socket socket		= this.serverSocket.accept();

			// Create a new thread to handle the request every time a connection
			// is accepted
			ServerThread thread	= new ServerThread
										(	socket,
											this.config.documentRoot,
											this.config.filesDirectory,
											this.config.enableAuth );
		}
	}
}
