/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Contains the implementation of the authentication system
 * Date:		11/26/2012
 */

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * An interface for creating panes in the configuration GUI
 *
 * @author Craig Hammond
 * @author Kritphong Mongkhonvanit
 */
public class GuiPane extends JPanel
{
	private static final long serialVersionUID = 1L;

	private static final int SPACING = 3;

	private JLabel statusLabel;
	private JPanel bottomPanel;
	private JButton saveButton;
	private JButton resetButton;
	private JButton exitButton;

	private JPanel mainPanel;

	/**
	 * Constriuctor
	 */
	public GuiPane()
	{
		FlowLayout panelLayout			= new FlowLayout(FlowLayout.LEFT);
		GridBagLayout layout			= new GridBagLayout();
		GridBagConstraints constraints	= new GridBagConstraints();
		JPanel buttonPanel				= new JPanel();

		panelLayout.setAlignOnBaseline(false);

		constraints.weightx = 0.5;
		constraints.weighty = 0.5;
		constraints.anchor	= GridBagConstraints.EAST;
		constraints.fill	= GridBagConstraints.BOTH;
		constraints.insets	= new Insets(5, 5, 5, 0);

		this.mainPanel		= new JPanel(layout);
		this.saveButton		= new JButton("Save");
		this.resetButton	= new JButton("Reset");
		this.exitButton		= new JButton("Exit");
		this.statusLabel 	= new JLabel("Status - ");
		this.bottomPanel	= new JPanel(new BorderLayout());

		buttonPanel.add(resetButton);
		buttonPanel.add(saveButton);
		buttonPanel.add(exitButton);
		bottomPanel.add(statusLabel, BorderLayout.NORTH);
		bottomPanel.add(buttonPanel, BorderLayout.SOUTH);

		this.setLayout(new BorderLayout());
		this.add(mainPanel, BorderLayout.NORTH);
		this.add(bottomPanel, BorderLayout.SOUTH);
	}

	/**
	 * Sets the status to the given string
	 *
	 * @param	status	The status that should be set
	 */
	public void setStatus(String status)
	{
		statusLabel.setText("Status - "+status);
	}

	/**
	 * Adds a component to the main panel with the given attributes
	 *
	 * @param	component	The component to add
	 * @param	constraints	The contraints to use
	 * @param	x			The x position
	 * @param	y			The y position
	 * @param	width		The width it should take
	 * @param	height		The height it should take
	 */
	public void addToContainer(	JComponent component,
								GridBagConstraints constraints,
								int x,
								int y,
								int width,
								int height )
	{
		constraints.gridx		= x;
		constraints.gridy		= y;
		constraints.gridwidth	= width;
		constraints.gridheight	= height;

		this.mainPanel.add(component, constraints);
	}

	/**
	 * Allows the owner to have access to the buttons
	 */
	public void setActionListener(ActionListener actionListener)
	{
		this.saveButton.addActionListener(actionListener);
		this.resetButton.addActionListener(actionListener);
		this.exitButton.addActionListener(actionListener);
	}

	/**
	 * Reset button getter
	 */
	public JButton getResetButton()
	{
		return this.resetButton;
	}

	/**
	 * Save button getter
	 */
	public JButton getSaveButton()
	{
		return this.saveButton;
	}

	/**
	 * Exit button getter
	 */
	public JButton getExitButton()
	{
		return this.exitButton;
	}
}
