function generateHTML(fileList)
{
	var result	= new String();
	var fileArray	= fileList.split("\r\n");

	for(i in fileArray)
	{
		if(fileArray[i].length > 0)
		{
			result +=	"<a href=\"files/"+
					fileArray[i]+
					"\">"+
					fileArray[i]+
					"</a><br>";
		}
	}

	return result.slice(0, -4);
}

function init()
{
	var filelistdiv	= document.getElementById("filelistdiv");
	var xhr		= new XMLHttpRequest();

	xhr.open("GET", "files", false);
	xhr.send();

	filelistdiv.innerHTML = generateHTML(xhr.responseText);
}
