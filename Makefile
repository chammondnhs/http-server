JAVAC	= javac
JAR	= jar
RM	= rm -f
WGET	= wget

SQLITE_JDBC = sqlite-jdbc.jar

SQLITE_JDBC_SRC = \
	"http://www.xerial.org/maven/repository/artifact/org/xerial/sqlite-jdbc"

ifeq ($(wildcard $(SQLITE_JDBC)),)
	SQLITE_JDBC_VER :=	$(shell $(WGET) -O - \
				$(SQLITE_JDBC_SRC)/maven-metadata.xml \
				| grep '<release>.*' \
				| sed 's/\s*<\/\?release>\s*//g')
endif

SQLITE_JDBC_URI = \
	$(SQLITE_JDBC_SRC)/$(SQLITE_JDBC_VER)/sqlite-jdbc-$(SQLITE_JDBC_VER).jar

JAVASRC =	Authenticator.java AccessPane.java AutoComboBox.java \
		ConfigParser.java HttpShare.java ServerConfig.java \
		GroupPane.java GuiPane.java ConfigPane.java UserPane.java \
		Server.java ServerThread.java HttpRequest.java Database.java

JAVACLASS	= $(shell echo $(JAVASRC) | sed 's/\.java[ ]\?/.class /g')
JARFILE		= HttpShare.jar
MANIFEST	= Manifest

all: $(JAVASRC) $(JAVACLASS) $(JARFILE) $(SQLITE_JDBC)

$(JARFILE): $(JAVACLASS) $(MANIFEST)
	$(JAR) cmf $(MANIFEST) $(JARFILE) $(JAVACLASS)

$(JAVACLASS): $(JAVASRC)
	$(JAVAC) $(shell echo $@ | sed 's/\.class$$/.java/')

$(SQLITE_JDBC):
	$(WGET) -O $(SQLITE_JDBC) $(SQLITE_JDBC_URI)

clean:
	$(RM) $(JAVACLASS) $(SQLITE_JDBC)

.PHONY: clean
