/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	A class for parsing HTTP request headers
 * Date:		10/8/2012
 */

import java.io.InputStream;
import java.io.IOException;

/**
 * Parse the request header
 *
 * @author	Kritphong Mongkhonvanit
 * @author	Craig Hammond
 * @version	1.0	10/8/2012
 */
public class HttpRequest
{
	private static final String EOL = "\r\n"; // mandated by RFC 2616

	private String method;	// Request method
	private String URI;		// Request URI

	private String contentType;
	private String boundary;

	private String username;
	private String password;

	private int contentLength;

	// defaults to -1 so that there's a way to tell when range is not supplied
	private int rangeStart	= -1;	// Start of the requested range
	private int rangeEnd	= -1;	// End of the requested range

	/**
	 * Constructor - Parses the request header and set the member variables to
	 * the corresponding values
	 *
	 * @param	input	Input stream of the request
	 */
	public HttpRequest(InputStream input) throws IOException, Exception
	{
		String header			= new String();
		char current			= Character.UNASSIGNED;
		boolean done			= false;
		boolean foundNewLine	= false;
		boolean newLineCheck	= false;
		int newLineCheckPos		= 1;

		while(!done)
		{
			// the header is all text so it's safe to cast input.read() to char
			current = (char)input.read();

			if(current == EOL.charAt(0))
			{
				if(EOL.length() == 1)
				{
					if(foundNewLine)
					{
						done = true;
					}
					else
					{
						foundNewLine = true;
					}
				}
				else
				{
					newLineCheck = true;
				}
			}
			else if(newLineCheck)
			{
				if(current != EOL.charAt(newLineCheckPos))
				{
					foundNewLine	= false;
					newLineCheck	= false;
				}
				else if(newLineCheckPos == EOL.length()-1)
				{
					if(foundNewLine) // found blank line
					{
						done = true;
					}
					else
					{
						foundNewLine = true;
					}

					newLineCheckPos = 1;
				}
				else
				{
					newLineCheckPos++;
				}
			}

			header += current;
		}

		this.parse(header);

		// throw an exception if range is invalid
		if(rangeStart > rangeEnd)
		{
			throw new Exception("Invalid HTTP request range");
		}
	}

	/**
	 * Get a line from input stream.
	 *
	 * @param	input		The stream of data to get a line from
	 * @return				A line from the stream
	 */
	private static String getLine(InputStream input) throws IOException
	{
		String line				= new String();
		String newLine			= EOL;
		char current			= Character.UNASSIGNED;
		boolean done			= false;
		boolean newLineCheck	= false;
		int newLineCheckPos		= 0;

		while(!done)
		{
			// the header is all text so it's safe to cast input.read() to char
			current = (char)input.read();

			if(current == EOL.charAt(0))
			{
				if(EOL.length() == 1)
				{
					done = true;
				}
				else
				{
					newLineCheck = true;
				}
			}
			else if(newLineCheck)
			{
				if(current != EOL.charAt(newLineCheckPos))
				{
					newLineCheck = false;
				}
				else if(newLineCheckPos == EOL.length()-1)
				{
					done = true;
				}
				else
				{
					newLineCheckPos++;
				}
			}

			line += current;
		}

		return line.substring(0, line.length()-newLine.length()-1);
	}

	/**
	 * Base64 decode data
	 *
	 * @param	input	The data to decode
	 */
	public static byte[] base64Decode(String input) throws Exception
	{
		byte[] result = new byte[input.length()*3/4];
		byte[] inputBytes = input.getBytes();

		for(int i = 0; i < inputBytes.length; i += 4)
		{
			byte[] resultBytes	= new byte[3];

			for(int j = i; j < i+4; j++)
			{
				if(inputBytes[j] == 61)
				{
					inputBytes[j] = 0;
				}
				else if(inputBytes[j] == 43)
				{
					inputBytes[j] = 62;
				}
				else if(inputBytes[j] == 47)
				{
					inputBytes[j] = 63;
				}
				else if(inputBytes[j] <= 57)
				{
					inputBytes[j] += 4;
				}
				else if(inputBytes[j] <= 90)
				{
					inputBytes[j] -= 65;
				}
				else if(inputBytes[j] <= 122)
				{
					inputBytes[j] -= 71;
				}
				else // Invalid character
				{
					throw new Exception("Error decoding base64 string");
				}
			}

			resultBytes[0]	=	(byte)(inputBytes[i] << 2);
			resultBytes[0]	|=	(byte)((inputBytes[i+1] >> 4));

			resultBytes[1]	=	(byte)(inputBytes[i+1] << 4);
			resultBytes[1]	|=	(byte)((inputBytes[i+2] >> 2));

			resultBytes[2]	=	(byte)(inputBytes[i+2] << 6);
			resultBytes[2] 	|=	inputBytes[i+3];

			result[(i/4)*3]		= resultBytes[0];
			result[(i/4)*3+1]	= resultBytes[1];
			result[(i/4)*3+2]	= resultBytes[2];
		}

		return result;
	}

	/**
	 * Parse the request header
	 *
	 * @param	header	The request header to parse
	 */
	private void parse(String header) throws Exception
	{
		int currentLine		= 0;
		String lineList[]	= header.split(EOL);
		String line			= lineList[currentLine];
		String[] tokens		= line.split(" ");

		this.URI	= tokens[1];
		this.method	= tokens[0];

		while(currentLine < lineList.length)
		{
			line = lineList[currentLine++];

			if(line.matches("Range:\\s*bytes\\s*=\\s*.*"))
			{
				this.parseRange(line);
			}
			else if(line.matches("Content-Type:.*boundary=.*"))
			{
				this.parseContentType(line);
			}
			else if(line.matches("Content-Length:\\s*[0-9]*"))
			{
				this.parseContentLength(line);
			}
			else if(line.matches("Authorization:\\s*Basic\\s*.*"))
			{
				this.parseCredentials(line);
			}
		}
	}

	/**
	 * Parse string representing content length
	 *
	 * @param	line	The string to be parsed
	 */
	private void parseContentLength(String line)
	{
		String length = line.replaceAll("Content-Length:\\s*", "");

		this.contentLength = Integer.parseInt(length);
	}

	/**
	 * Parse string representing a range
	 *
	 * @param	line	The string to be parsed
	 */
	private void parseRange(String line) throws Exception
	{
		// Array containing the start and the end of the range
		String[] tokens = line.replaceAll(".*:.*=\\s*", "").split("-");

		this.rangeStart	= Integer.parseInt(tokens[0]);

		if(tokens.length == 2)
		{
			this.rangeEnd = Integer.parseInt(tokens[1]);
		}
		else if(tokens.length != 1)
		{
			throw new Exception("Error parsing HTTP request range");
		}
	}

	/**
	 * Parse user credentials string
	 *
	 * @param	line	The string to parse
	 */
	 private void parseCredentials(String line) throws Exception
	 {
		String credentials			= line.replaceAll
										("Authorization:\\s*Basic\\s*", "");

		String decodedCredentials	= new String
											(HttpRequest.base64Decode
												(credentials) )
											.trim();

		int separatorIndex			= decodedCredentials.indexOf(':');

		this.username	= decodedCredentials.substring(0, separatorIndex);
		this.password	= decodedCredentials.substring(separatorIndex+1);
	 }

	/**
	 * Accessor for method
	 */
	private void parseContentType(String line)
	{
		this.contentType	= line
								.replaceAll("Content-Type:\\s*", "")
								.replaceAll(",.*", "");

		this.boundary		= line.replaceAll(".*boundary=", "");
	}

	/**
	 * Accessor for method
	 */
	public String getMethod()
	{
		return this.method;
	}

	/**
	 * Accessor for URI
	 */
	public String getURI()
	{
		return this.URI;
	}

	/**
	 * Accessor for rangeStart
	 */
	public String getContentType()
	{
		return contentType;
	}

	/**
	 * Accessor for boundary
	 */
	public String getBoundary()
	{
		return boundary;
	}

	/**
	 * Accessor for contentLength
	 */
	public int getContentLength()
	{
		return contentLength;
	}

	/**
	 * Accessor for rangeStart
	 */
	public int getRangeStart()
	{
		return this.rangeStart;
	}

	/**
	 * Accessor for rangeEnd
	 */
	public int getRangeEnd()
	{
		return this.rangeEnd;
	}

	/**
	 * Accessor for username
	 */
	public String getUsername()
	{
		return this.username;
	}

	/**
	 * Accessor for password
	 */
	public String getPassword()
	{
		return this.password;
	}
}
