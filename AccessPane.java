/**
 * Name: 		Craig Hammond
 * Name:		Kritphong Mongkhonvanit
 * Description:	Contains the implementation of the authenticator system
 * Date:		12/27/2012
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.lang.ClassNotFoundException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;

/**
 * The tab pane for modifying user access
 *
 * @author Craig Hammond
 * @author Kritphong Mongkhonvanit
 */
public class AccessPane extends GuiPane implements ActionListener
{
	private static final long serialVersionUID = 1L;

	private AutoComboBox userAccessUser;
	private AutoComboBox userAccessGroup;
	private AutoComboBox groupAccessGroup;
	private AutoComboBox groupAccessFile;

	private JButton userAccessButton;
	private JButton groupAccessButton;

	private Database database;

	/**
	 * Constructor
	 */
	public AccessPane() throws SQLException, ClassNotFoundException
	{
		GridBagConstraints constraints = new GridBagConstraints();

		this.database = new Database();

		this.userAccessUser		= new AutoComboBox
										(this.database.getUserList());

		this.userAccessGroup	= new AutoComboBox
										(this.database.getGroupList());

		this.groupAccessGroup	= new AutoComboBox
										(this.database.getGroupList());

		this.groupAccessFile	= new AutoComboBox
										(this.database.getFileList());

		this.userAccessButton	= new JButton("Add");
		this.groupAccessButton	= new JButton("Add");

		this.getSaveButton().setVisible(false);
		this.getResetButton().setVisible(false);
		this.getExitButton().setVisible(false);

		constraints.fill	= GridBagConstraints.BOTH;
		constraints.anchor	= GridBagConstraints.EAST;
		constraints.weightx	= 0.25;
		constraints.weighty	= 0.5;
		constraints.insets	= new Insets(5, 5, 0, 5);

		this.addToContainer
				(new JLabel("Username") , constraints, 0, 0, 1, 1);

		this.addToContainer
				(new JLabel("Group") , constraints, 1, 0, 1, 1);

		this.addToContainer
				(new JLabel("Group") , constraints, 0, 3, 1, 1);

		this.addToContainer
				(new JLabel("File") , constraints, 1, 3, 1, 1);

		this.addToContainer(this.userAccessUser, constraints, 0, 1, 1, 1);
		this.addToContainer(this.userAccessGroup, constraints, 1, 1, 1, 1);
		this.addToContainer(this.groupAccessGroup, constraints, 0, 5, 1, 1);
		this.addToContainer(this.groupAccessFile, constraints, 1, 5, 1, 1);

		constraints.fill = GridBagConstraints.VERTICAL;

		this.addToContainer(this.userAccessButton, constraints, 1, 2, 1, 1);
		this.addToContainer(this.groupAccessButton, constraints, 1, 6, 1, 1);

		this.userAccessButton.addActionListener(this);
		this.groupAccessButton.addActionListener(this);

		this.setActionListener(this);
	}

	/**
	 * Updates the combo boxes with the latest changes to the
	 * database
	 */
	public void update() throws SQLException
	{
		this.userAccessUser.setContent(database.getUserList());
		this.userAccessGroup.setContent(database.getGroupList());
		this.groupAccessGroup.setContent(database.getGroupList());
		this.groupAccessFile.setContent(database.getFileList());
	}

	/**
	 * Handles reset button press
	 */
	private void resetHandler() throws SQLException
	{
		this.update();
	}

	/**
	 * Handles save button press
	 */
	private void saveHandler() throws NoSuchAlgorithmException, SQLException
	{
		if(	this.userAccessUser.getSelectedIndex() == -1 
			|| this.userAccessGroup.getSelectedIndex() == -1 )
		{
			this.resetHandler();
		}
		else
		{
			String currentUser	= this
									.userAccessUser
									.getSelectedItem()
									.toString();

			String currentGroup = this
									.userAccessGroup
									.getSelectedItem()
									.toString();

			this.setStatus(currentUser+" has been placed in "+currentGroup);

			this.database.addUserAccess(currentUser, currentGroup);

			this.update();
		}
	}

	/**
	 * Adds a new user-group relationship to the database
	 */
	public void addUserAccess() throws SQLException
	{
		String currentUser	= this
								.userAccessUser
								.getSelectedItem()
								.toString();

		String currentGroup = this
								.userAccessGroup
								.getSelectedItem()
								.toString();

		this.database.addUserAccess(currentUser, currentGroup);

		this.setStatus(currentUser+" has been placed in "+currentGroup);
	}

	/**
	 * Adds a new group-file relationship to the database
	 */
	public void addGroupAccess() throws SQLException
	{
		String currentFile	= this
								.groupAccessFile
								.getSelectedItem()
								.toString();

		String currentGroup = this
								.groupAccessGroup
								.getSelectedItem()
								.toString();

		this.database.addGroupAccess(currentGroup, currentFile);

		this.setStatus(currentFile+" has been placed in "+currentGroup);
	}

	/**
	 * Handles actions
	 *
	 * @param	event	The event object associated with the event
	 */
	public void actionPerformed(ActionEvent event)
	{
		try
		{
			if(event.getSource() == this.userAccessButton)
			{
				this.addUserAccess();
			}
			else if(event.getSource() == this.groupAccessButton)
			{
				this.addGroupAccess();
			}
			else if(event.getSource() == this.getExitButton())
			{
				System.exit(0);
			}
		}
		catch(Exception exception)
		{
			System.err.println(exception.getMessage());
		}
	}
}
